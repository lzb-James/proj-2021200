compile Tensorflow on Linux
=====

`JDK1.8.0`
*  **sudo add-apt-repository ppa:webupd8team/java**
*  **sudo apt-get update**
*  **sudo apt-get install oracle-java8-installer**


tensorflow最适合的版本是python3.5


*  **sudo apt-get update**


*  **sudo apt-get install python3.5**


*  **sudo cp /usr/bin/python /usr/bin/python_bak**


*  **sudo rm /usr/bin/python**


*  **sudo ln -s /usr/bin/python3.5 /usr/bin/python**

*  **sudo apt install python3.5-dev** 

Bazel
====

Bazel有如下特点：

`Bazel是一个类似于Make的工具，是Google为其内部软件开发的特点量身定制的工具，如今Google使用它来构建内部大多数的软件。它的功能有诸多亮点：`
*  多语言支持：目前Bazel默认支持Java、Objective-C和C++，但可以被扩展到其他任何变成语言。
*  高级构建描述语言：项目是使用一种叫BUILD的语言来描述的，它是一种简洁的文本语言，它把一个项目视为一个集合，这个集合由一些互相关联的库、二进制文件和测试用例组成。相反，像Make这样的工具，需要去描述每个文件如何调用编译器。
*  多平台支持：同一套工具和相同的BUILD文件可以用来为不同的体系结构构建软件，甚至是不同的平台。在Google，Bazel被同时用在数据中心系统中的服务器应用和手机端的移动应用上。
*  可重复性：在BUILD文件中，每个库、测试用例和二进制文件都需要明确指定它们的依赖关系。当一个源码文件被修改时，Bazel凭这些依赖来判断哪些部分需要重新构建，以及哪些任务可以并行进行。这意味着所有构建都是增量的，并且相同构建总是产生一样的结果。
*  可伸缩性：Bazel可以处理大型项目；在Google，一个服务器软件有十万行代码是很常见的，在什么都不改的前提下重新构建这样一个项目，大概只需要200毫秒。
   

    对于为什么要重新发明一个构建工具而不直接使用Make，Google认为Make控制得太细，最终的结果完全依靠开发人员能正确编写规则。很久以前，Google使用自动生成的臃肿的Makefile来构建他们的软件，速度太慢，结果不可靠，最终影响了研发人员的效率和公司的敏捷性。所以他们做了Bazel。Bazel的规则层次更高，比如，对于“Java测试”、“C++二进制文件”，它都有定义好的内建规则，而这些规则都已经被无数的测试证明是正确和稳定的。


*  **echo "deb [arch=amd64] http://storage.googleapis.com/bazel-apt stable jdk1.8" | sudo tee /etc/apt/sources.list.d/bazel.list**
*  **curl https://bazel.build/bazel-release.pub.gpg | sudo apt-key add -**
*  **sudo apt-get update && sudo apt-get install bazel**
*  **sudo apt-get upgrade bazel**


下载tensorflow
====
`git clone --recurse-submodules https://github.com/tensorflow/tensorflow`

*  **cd  ~/tensorflow**
    
*  **sudo ./configure**

然后会询问你是否安装相关环境以及python console 的位置：按照要求安装即可

接下来通过Bazel来编译tensorflow。

*  **bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package**

会在pkg路径下生成一个whl类型的压缩文件

然后 pip install 当前路径下的.whl文件。

测试：

    >>>python
    >>> import tensorflow as tf
    >>> hello = tf.constant("Hello, TensorFlow!")
    >>> sess = tf.Session()
    >>> print(sess.run(hello))
    b'Hello World, TensorFlow!'

