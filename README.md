项目的简介
=======


项目名称：

**在 openEuler aarch64 架构上完成 Tensorflow 基于公开数据集完成 mnist 训练过程**

Introduce：

**描述：openEuler是华为旗下一个开源免费的Linux发行版系统，通过开放的社区形式与全球的开发者共同构建一个开放、多元和架构包容的软件生态体系，openEuler同时是一个创新的系统，倡导客户在系统上提出创新想法、开拓新思路、实践新方案。该项目意在让大家通过国产Linux系统openEuler从源码去搭建一个Tensorflow，并且训练Minist数据集，并打包成Rpm格式**

项目目的：

*  **从源码角度去搭建Tensorflow**

*  **让大家从操作系统的角度，用openeuler去训练Minist数据集**

*  **对AI算法有一个初步的了解**

*  **Rpm/Dnf包管理器的使用**


技术栈：

*  **Tensorflow**
*  **C++**
*  **Linux**
*  **AI算法**

关键词：

**AI算法,openEuler,Tensorflow,包管理**

![输入图片说明](https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2970761526,4095020438&fm=15&gp=0.jpg "在这里输入图片标题")